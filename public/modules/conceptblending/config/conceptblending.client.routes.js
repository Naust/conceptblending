'use strict';

//Setting up route
angular.module('conceptblending').config(['$stateProvider',
	function($stateProvider) {
		// Conceptblending state routing
		$stateProvider.
		state('find-instrument', {
			url: '/find-instrument',
			templateUrl: 'modules/conceptblending/views/find-instrument.client.view.html'
		}).
		state('conceptblending', {
			url: '/conceptblending',
			templateUrl: 'modules/conceptblending/views/conceptblending.client.view.html'
		});
	}
]);