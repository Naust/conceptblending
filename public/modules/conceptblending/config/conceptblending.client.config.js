'use strict';

// Conceptblending module config
angular.module('conceptblending').run(['Menus',
	function(Menus) {
		// Config logic
		// ...
		// Set top bar menu items
		Menus.addMenuItem('topbar', 'Concept blending', 'conceptblending', 'dropdown', '/conceptblending(/create)?');
		Menus.addSubMenuItem('topbar', 'conceptblending', 'Check word', 'conceptblending');
		Menus.addSubMenuItem('topbar', 'conceptblending', 'Find instruments', 'find-instrument');
	}
]);
