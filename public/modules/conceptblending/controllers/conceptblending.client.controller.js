'use strict';

angular.module('conceptblending').controller('ConceptblendingController', ['$scope', '$http',
	function($scope, $http) {
		// Controller Logic
		// ...
		// Find a list of Categories

		// look up word in wordnet
		$scope.requestWordLookup = function() {
			console.log('Requesting lookup of word: ' + this.inputword);
				$http.post('/word',{'word' : this.inputword}).then(
					function(response) {
						console.log(response);
						console.log(response.data);
						$scope.word = response.data;
						// Clear form fields
						$scope.inputword = '';
					},
					function(response){
						console.log('error:');
						console.log(response);
					}
				);
		};


		$scope.find = function() {
			// hard coded data
			$scope.word = {
				'word': 'Not loaded',
				'type': 'Not loaded'
			};
			$http.get('/word').then(
				function(response) {
					console.log(response);
					console.log(response.data);
					$scope.word = response.data;
				},
				function(response){
					console.log('error:');
					console.log(response);
					$scope.word = {
						'word': 'Error',
						'type': 'Error'
					};
				}
			);

		};
	}
]);
