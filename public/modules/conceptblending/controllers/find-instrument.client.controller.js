'use strict';

/*global $:false */

angular.module('conceptblending').controller('FindInstrumentController', ['$scope', '$http', 'Removestopwords', 'Parser', 'Tree',
	function($scope, $http, Removestopwords, Parser, Tree) {
		// Find instrument controller logic
		// ...

		/*var console = {};
		console.log = function(){};*/

		//Concept data structure
		function Concept(wikipediaAbstract, wikipediaAbstractWithoutStopwords, nounPhrases, properties) {
			this.wikipediaAbstract = wikipediaAbstract || '';
			this.wikipediaAbstractWithoutStopwords = wikipediaAbstractWithoutStopwords || '';
			this.nounPhrases = nounPhrases || '';
			this.properties = properties || [];
		}

		$scope.concepts = [
			new Concept(),
			new Concept()
		];

		//Graph data
		$scope.networkDatasets = [{},{},{},{}];
		$scope.networkOptions = {
				layout: {
						hierarchical: {
								direction: 'DU',
								sortMethod: 'directed',
								blockShifting: true,
								edgeMinimization: true,
								parentCentralization: true,
								nodeSpacing: 150
						}
				},
				interaction: {dragNodes :true},
				physics: {
						enabled: false
				}
		};

		//Find a concept on wikipedia
		$scope.wikipediaPageName = ['Cajón','Cajón'];
		var randomInstruments = ['Agung a Tamlang', 'Bamboo slit drum', 'Balafon', 'Cajón', 'Castanets', 'Clapsticks', 'Calve sticks', 'Glockenspiel', 'Glownwaren', 'Handpan',
		'Hang (instrument)', 'Marimba', 'Steelpan', 'Triangle (musical instrument)', 'Vibraphone', 'Wood block', 'Xylophone', 'Agida'];
		$scope.maxWordsToRetrieve = 30;
		$scope.maxSynsetsAllowedPerProperty = 10;
		$scope.conceptIsInstrument = true;
		var normalizeScoreByWordLength = false;

		//Find Matches
		$scope.matches = [];
		$scope.minimumDepth = 0; //How specialized the match can be
		$scope.maximumHeight = 10; //how abstract the match can be

		//Creative strategy
		$scope.subtrees = [[],[]];
		$scope.activatedForBlending = [[],[]];
		$scope.isSelectAllActivated = [false, false];
		$scope.conceptToKeepInBlend = 0;

		//Swap trees
		$scope.swappedSubtrees = [[],[]];
		$scope.nodeString = [[],[]];

		var penaltyCategories = [100002137, 103806455, 100007846];

		$scope.pickRandomInstrument = function(concept) { $scope.wikipediaPageName[concept] = randomInstruments[Math.floor(Math.random() * randomInstruments.length)]; };

		$scope.findConceptFromWikipedia = function(concept) {
			//Get Leading paragraphs (section 0)
			$.getJSON('http://en.wikipedia.org/w/api.php?action=parse&page=' + $scope.wikipediaPageName[concept] + '&prop=text&section=0&format=json&redirects=1&callback=?', function (data) {
				if (typeof data.parse.text === 'undefined' || data.parse.text === null) console.log('Error');
				else
			    for (var entry in data.parse.text) {
			        var pText = Parser.getAbstractFromWikipediaArticle(data.parse.text[entry].split('<p>'));

							var wikipediaAbstract = Parser.removeWikipediaArtifacts(pText);
							var wikipediaAbstractWithoutStopwords = Removestopwords.removeStopwords(Parser.removeSymbolsWordNetWontAccept(wikipediaAbstract));
							wikipediaAbstractWithoutStopwords = Parser.removeDuplicateWords(wikipediaAbstractWithoutStopwords);

							$scope.concepts[concept].wikipediaAbstract = wikipediaAbstract;
			        $scope.concepts[concept].wikipediaAbstractWithoutStopwords = wikipediaAbstractWithoutStopwords;
							$scope.concepts[concept].nounPhrases = '';
							$scope.$apply();
			    }
			});
		};

		$scope.requestSynsetsForEachProperty = function(concept, maxWordsToRetrieve, maxSynsetsAllowedPerProperty) {
			var propertyWords = this.concepts[concept].wikipediaAbstractWithoutStopwords.split(' ').splice(0,maxWordsToRetrieve);
			if (this.concepts[concept].nounPhrases.length > 0) propertyWords = propertyWords.concat(this.concepts[concept].nounPhrases.split('\n'));
			console.log(propertyWords);

			$http.post('/synsets', {
				'propertyWords' : propertyWords,
				'wikipediaAbstract' : this.concepts[concept].wikipediaAbstract
			}).then(
				function(response) {
					for (var i = 0; i < propertyWords.length; i++) {
						if (propertyWords[i] === '') continue;
						var multipleSynsets = (response.data.synsets[i].length > 1);
						var oneSynset = (response.data.synsets[i].length === 1);
						var topSynset = multipleSynsets ?
							getSynsetWithHighestScore(response.data.lemmas, response.data.synsets[i], i, maxSynsetsAllowedPerProperty) :
							{ key: oneSynset ? 0 : -1 };

						var property = {
							'word': propertyWords[i],
							'lemmas': response.data.lemmas[i],
							'pos': response.data.pos[i],
							'synsets': response.data.synsets[i],
							'hypernyms': response.data.hypernyms[i],
							'topSynset': topSynset.key,
							'lastTopSynset': -1
						};

						if ($scope.isInAPenalizingCategory(property))
							property.topSynset = -1;

						$scope.concepts[concept].properties.push(property);

						if (multipleSynsets && topSynset.key !== -1) $scope.requestHypernyms(i, propertyWords[i], topSynset.synsetID, concept);
					}
				},
				function(response){
					console.error('error:', response);
				}
			);
		};

		$scope.requestNounPhrases = function(concept) {
			$http.post('/nounphrases',{
				'wikipediaAbstract' : this.concepts[concept].wikipediaAbstract
			}).then(
				function(response) {
					console.log(response);
					$scope.concepts[concept].nounPhrases = response.data.nounphrases.join('\n');
				},
				function(response) {
					console.error('error:', response);
				}
			);
		};

		$scope.updateSynsetsByHypernyms = function(concept) {
			var i;
			var hypernyms = [];
			var splitAndSend = function(elem) { //Split multiple hypernyms and send elements
				hypernyms.push(elem.lemma);
			};
			var properties = $scope.concepts[concept].properties;
			for (i = 0; i < properties.length; i++) {
				if (typeof properties[i].hypernyms !== 'undefined' && typeof properties[i].hypernyms[0] !== 'undefined') properties[i].hypernyms[0].words.forEach(splitAndSend);
			}
			var abstractLemmas = [];
			properties.forEach(function(property) {
				if (property.lemmas !== '')
					property.lemmas.split(', ').forEach(function(lemma) {
						abstractLemmas.push(property.lemmas);
					});
			});

			for (i = 0; i < properties.length; i++) {
				if (properties[i] !== '') {
					if (properties[i].synsets.length <= 1) properties[i].topSynset = 0;
					else {
						var topSynset = getSynsetWithHighestScore(abstractLemmas, properties[i].synsets, i, $scope.maxSynsetsAllowedPerProperty, hypernyms);
						properties[i].lastTopSynset = properties[i].topSynset;
						properties[i].topSynset = topSynset.key;
						if (topSynset.key !== -1) $scope.requestHypernyms(i, properties[i].word, topSynset.synsetID, concept);
					}
				}
			}
		};

		var getSynsetWithHighestScore = function(abstractLemmas, synsets, avoidKey, maxSynsetsAllowedPerProperty, hypernyms) {
			if (synsets.length > maxSynsetsAllowedPerProperty)
				return {
					key: -1,
					synsetID: -1
				};
			var i, j, k;
			var synsetScore = 0;
			var synsetMaxScore = 0;
			var topSynsetKey = -1;
			var wordsInDefinition = [];
			var contextLemmas = [];

			if ($scope.conceptIsInstrument)
				contextLemmas = ['music', 'instrument', 'tone', 'tones', 'sounds', 'sounding', 'rhythm', 'melody', 'drum', 'flute', 'percussion', 'idiophone',
						'bass', 'baritone', 'tenor', 'alto', 'soprano', 'note', 'pitch', 'audio', 'auditory', 'tune', 'tuning', 'acoustic'];
			var wordsInAbstract = JSON.parse(JSON.stringify(abstractLemmas)); //Removes empty strings from array
			contextLemmas.forEach(function(elem) { //Push context word if not duplicate
				if (wordsInAbstract.indexOf(elem) === -1) wordsInAbstract.push(elem);
			});
			if (typeof hypernyms !== 'undefined')
				hypernyms.forEach(function(elem) { //Push hypernym word if not duplicate
					if (wordsInAbstract.indexOf(elem) === -1) wordsInAbstract.push(elem);
				});

			for (i = 0; i < synsets.length; i++) { //Go through each definition
				synsetScore = 0;
				wordsInDefinition = Removestopwords.removeStopwords(synsets[i].definition).split(' '); //Find each word except stopwords
				if (wordsInDefinition[0] !== '')
				for (j = 0; j < wordsInDefinition.length; j++) { //For each word
					for (k = 0; k < wordsInAbstract.length; k++) { //Check in every word of the abstract
						if (k !== avoidKey && //Avoid the definition word
								wordsInAbstract[k].length > 0 &&
									(/*wordsInAbstract[k].indexOf(wordsInDefinition[j]) !== -1 ||*/
									wordsInAbstract[k].indexOf(wordsInDefinition[j]) === 0 || //Finds word in start, but not end (act is first in action but not first in interact)
									wordsInDefinition[j].indexOf(wordsInAbstract[k]) === 0)) { //To see if it exists there too
							synsetScore++;
							wordsInDefinition[j].split(', '), wordsInAbstract[k]);
							wordsInDefinition[j], wordsInAbstract[k]);
						}
					}
				}
				else console.log(words.length + ' - ' + words[0].length);
				if (normalizeScoreByWordLength) synsetScore = synsetScore/wordsInDefinition.length; //Normalize by definition word length
				console.log(wordsInDefinition[0] + ' ' + wordsInDefinition[1] + ': ' + synsetScore + ' > ' + synsetMaxScore);
				if (synsetScore > synsetMaxScore) {
					synsetMaxScore = synsetScore;
					topSynsetKey = i;
				}
			}
			if (topSynsetKey === -1)
				return {
					key: -1,
					synsetID: -1
				};
			console.log('The winner is: ' + synsets[topSynsetKey].definition);
			console.log('Score: ' + synsetMaxScore);
			return {
				key: topSynsetKey,
				synsetID: synsets[topSynsetKey].synsetID
			};
		};

		$scope.requestHypernyms = function(key, word, synsetid, concept) {
			console.log('Requesting hypernyms');
			var property = $scope.concepts[concept].properties[key];
			property.hypernyms = [];
			property.loadingHypernyms = true;

			$http.post('/hypernyms',{
				'word': word,
				'synsetid': synsetid
			}).then(
				function(response) {
					console.log(response);
					property.hypernyms = response.data.hypernyms;
					if ($scope.isInAPenalizingCategory(property)) {
						property.lastTopSynset = property.topSynset;
						property.topSynset = -1;
						property.hypernyms = [];
					}
					property.loadingHypernyms = false;
				},
				function(response){
					console.error('error:', response);
					property.loadingHypernyms = false;
				}
			);
		};

		$scope.initBlendToggles = function() {
			if ($scope.activatedForBlending[0].length !== 0 ||
					$scope.activatedForBlending[1].length !== 0) return;

			$scope.activatedForBlending[0].length = $scope.subtrees[0].length;
			$scope.activatedForBlending[1].length = $scope.subtrees[1].length;
			// $scope.activatedForBlending[0].fill(false);
			// $scope.activatedForBlending[1].fill(true);
			/*$scope.activatedForBlending[0].fill(true);
			$scope.activatedForBlending[1].fill(false);
			$scope.activatedForBlending[1][0] = true;*/
			$scope.activatedForBlending[0].fill(false);
			$scope.activatedForBlending[1].fill(false);
			var topConcept = ($scope.subtrees[0][0].depth - $scope.subtrees[0][0].branchLength - $scope.subtrees[0][0].penalty ) > ($scope.subtrees[1][0].depth - $scope.subtrees[1][0].branchLength - $scope.subtrees[1][0].penalty) ? 0 : 1;
			$scope.activatedForBlending[topConcept][0] = true;
		};

		$scope.toggleAllBlends = function(concept) {
			$scope.activatedForBlending[concept].length = $scope.subtrees[concept].length;
			$scope.activatedForBlending[concept].fill(!$scope.isSelectAllActivated[concept]);
		};

		$scope.findMatches = function() {
			$scope.matches = [];

			var findAllSynsets = function(properties, minimumDepth, maximumHeight) {
				var synsets = [];
				for (var i = 0; i < properties.length; i++) {
					if (properties[i].topSynset === -1) continue;
					if (typeof properties[i].synsets[properties[i].topSynset] === 'undefined') continue;
					var leaf = {
						'word': properties[i].word,
						'synset': properties[i].synsets[properties[i].topSynset].synsetID,
						'definition': properties[i].synsets[properties[i].topSynset].definition,
						'hypernyms': properties[i].hypernyms,
						'depth': (typeof properties[i].hypernyms === 'undefined' ? 0 : properties[i].hypernyms.length),
						'height': 0
					};
					if (leaf.synset[0] !== 'E' &&
							leaf.depth >= minimumDepth &&	leaf.height <= maximumHeight) synsets.push(leaf);
					if (typeof properties[i].hypernyms !== 'undefined')
						for (var j = 0; j < properties[i].hypernyms.length; j++) {
							var hypernym = {
								'word': leaf.hypernyms[j].words[0].lemma,
								'synset': leaf.hypernyms[j].synsetid,
								'definition': leaf.hypernyms[j].definition,
								'hypernyms': leaf.hypernyms.slice(j + 1),
								'depth': (typeof properties[i].hypernyms === 'undefined' ? 0 : properties[i].hypernyms.length - j - 1),
								'height': j + 1
							};
							if (hypernym.depth >= minimumDepth &&	hypernym.height <= maximumHeight) synsets.push(hypernym);
						}
				}
				return synsets;
			};

			var source = findAllSynsets($scope.concepts[0].properties, this.minimumDepth, this.maximumHeight);
			var target = findAllSynsets($scope.concepts[1].properties, this.minimumDepth, this.maximumHeight);

			//Remove duplicates HACK
			var removeDuplicates = function(synsets) {
				var arr = {};
				for (var i = 0; i < synsets.length; i++)
						arr[synsets[i].synset] = synsets[i];
				var newArr = [];
				for (var key in arr)
						newArr.push(arr[key]);
				return newArr;
			};

			source = removeDuplicates(source);
			target = removeDuplicates(target);

			$scope.matches = setOperations(source, target).intersection;
		};

		$scope.saveConcept = function(concept, conceptName) {

			function saveText(text, filename){
				var a = document.createElement('a');
				a.setAttribute('href', 'data:text/plain;charset=utf-u,'+encodeURIComponent(text));
				a.setAttribute('download', filename);
				a.click();
			}

			saveText(JSON.stringify(concept), conceptName.concat('.json'));

				}
		};

		$scope.loadConcept1 = function(event) {
			loadConcept(0, event);
		};
		$scope.loadConcept2 = function(event) {
			loadConcept(1, event);
		};

		var loadConcept = function(c, event) {
			var input = event.target;
			var reader = new FileReader();
			reader.onload = function(){
				var text = reader.result;
				$scope.$apply(function() {
					$scope.concepts[c] = JSON.parse(reader.result);
					$scope.concepts[c].properties.forEach(function(property) {
						if ($scope.isInAPenalizingCategory(property)) {
							property.lastTopSynset = property.topSynset;
							property.topSynset = -1;
							property.hypernyms = [];
						}
					});
					// $scope.concepts[c].properties = $scope.concepts[c].properties.filter(function(property) {
					// 	return !($scope.isInAPenalizingCategory(property));
					// });
					$scope.wikipediaPageName[c] = input.value.split('C:').join('').split('\\').join('').split('fakepath').join('').split('.json').join(''); //HACK
					$scope.correctSynsets.length = $scope.concepts[c].properties.length;
					$scope.correctSynsets.fill(true);
				});
			};
			reader.readAsText(input.files[0]);
		}

		$scope.generateTree = function(dataset) {
			var data;
			switch (dataset) {
				case 0:
				case 1:
					data = $scope.generateGraphFromWords(dataset); break;
				case 2:
					data = $scope.generateGraphFromMatches($scope.matches, this.minimumDepth, this.maximumHeight); break;
				case 3:
					data = $scope.generateGraphFromSubtrees($scope.networkDatasets[2], this.subtrees[0]); break;
				case 4:
					data = $scope.generateGraphFromSubtrees($scope.networkDatasets[2], this.subtrees[1]); break;
				case 5:
					data = $scope.generateGraphFromSubtrees($scope.networkDatasets[$scope.conceptToKeepInBlend], this.swappedSubtrees[0]); break;
				case 6:
					data = $scope.generateGraphFromSubtrees($scope.networkDatasets[2], this.swappedSubtrees[1]); break;
			}
			$scope.networkDatasets[dataset] = data;
		};

		$scope.generateGraphFromSubtrees = function(matches, subtrees) {
			var nodes = [];
			var edges = [];

			//Find all objects and create nodes and edges for each.
			nodes = Tree.createNodes(subtrees);
			edges = Tree.createEdges(subtrees);

			//Hook subtrees to matches
			nodes = nodes.concat(matches.nodes);
			edges = edges.concat(matches.edges);

			//Remove duplicates HACK
			var arr = {};
			var len = nodes.length;
			for (var i = 0; i < len; i++)
					arr[nodes[i].id] = nodes[i];
			nodes = [];
			for (var key in arr)
					nodes.push(arr[key]);

			return {nodes:nodes, edges:edges};
		};

		$scope.generateGraphFromMatches = function(matches, minimumDepth, maximumHeight) {
			var nodes = [];
			var edges = [];

			for (var i = 0; i < matches.length; i++) {
				var node = {
					id: matches[i].synset,
					label: matches[i].word + '*',
					color: {
						background:	(matches[i].depth < minimumDepth ? 'red' : (matches[i].height > maximumHeight ? 'darkorange' : (matches[i].height > 0 ? 'yellow' : 'lime'))),
						border: 'black'
					}
				};

				var length;
				if (typeof matches[i].hypernyms === 'undefined' || matches[i].hypernyms.length === 0) length = 1;
				else length = matches[i].hypernyms.length + 1;
				for (var j = 0; j < length; j++) {
					var stop = false;
					for (var k = 0; k < nodes.length; k++) {
						var checkSynset = matches[i].synset;
						if (j !== 0) checkSynset = matches[i].hypernyms[j - 1].synsetid;
						if (checkSynset === nodes[k].id) {
							stop = true;
							break;
						}
					}
					if (stop) continue;
					if (j === 0) {
						nodes.push(node);
						if (typeof matches[i].hypernyms === 'undefined' || matches[i].hypernyms.length === 0) break;
						edges.push({
							from: matches[i].synset,
							to: matches[i].hypernyms[0].synsetid
						});
						continue;
					}
					nodes.push({
						id: matches[i].hypernyms[j - 1].synsetid,
						label: matches[i].hypernyms[j - 1].words[0].lemma,
						color: {
							background:	(matches[i].depth - j < minimumDepth ? 'red' : (matches[i].height + j + 1 - 1> maximumHeight ? 'darkorange' : 'yellow')),
							border: 'black'
						}
					});
					if (typeof matches[i].hypernyms === 'undefined' || j+1 - 1 >= matches[i].hypernyms.length) continue;
					edges.push({
						from: matches[i].hypernyms[j - 1].synsetid,
						to: matches[i].hypernyms[j + 1 - 1].synsetid
					});
				}
			}

			//Remove duplicates HACK
			var arr = {};
			var len = nodes.length;
			for (i = 0; i < len; i++)
					arr[nodes[i].id] = nodes[i];
			nodes = [];
			for (var key in arr)
					nodes.push(arr[key]);

			return {nodes:nodes, edges:edges};
		};

		$scope.generateGraphFromWords = function(concept) {
			var tree = [];
			var properties = $scope.concepts[concept].properties;
			for (var i = 0; i < properties.length; i++) {
				var synset = properties[i].synsets[properties[i].topSynset];
				if (typeof synset === 'undefined') continue;

				if (tree.length === 0) {
					tree.push(Tree.createNewTree(properties[i]));
					continue;
				}
				var fitsInTree = false;
				for (var j = 0; j < tree.length; j++) {
					if (Tree.synsetIsInTree(synset.synsetid, tree[j])) {
						fitsInTree = true;
						break;
					}
					else if (Tree.hypernymIsInTree(properties[i].hypernyms, tree[j])) {
						var degree = Tree.degreeOfHypernymInTree(properties[i].hypernyms, tree[j]);
						var hyponymTree = Tree.getHyponymsAsTree(properties[i], degree);

						tree[j] = {
							nodes: tree[j].nodes.concat(hyponymTree.nodes),
							edges: tree[j].edges.concat(hyponymTree.edges)
						};
						fitsInTree = true;
						break;
					}
				}
				if (!fitsInTree) tree.push(Tree.createNewTree(properties[i]));
			}

			//Combine all trees into one object
			var nodes = [];
			var edges = [];
			for (i = 0; i < tree.length; i++) {
				nodes = nodes.concat(tree[i].nodes);
				edges = edges.concat(tree[i].edges);
			}

			//Remove duplicates HACK
			var arr = {};
			var len = nodes.length;
			for (i = 0; i < len; i++)
			    arr[nodes[i].id] = nodes[i];
			nodes = [];
			for (var key in arr)
			    nodes.push(arr[key]);

			return {nodes:nodes, edges:edges};
		};

		$scope.findAllCreativeSubtreesFromMatchedTree = function(concept) {
			var subtrees = [];
			//Find all subtrees roots in concepts[concept]
			var properties = $scope.concepts[concept].properties.filter(function(property) {
				return !(property.word.toUpperCase() === $scope.wikipediaPageName[concept].toUpperCase() ||
				property.topSynset === -1 ||
				synsetIsInArray(property.synsets[property.topSynset].synsetID, $scope.matches) || //Matches cant be swapped
				typeof property.hypernyms === 'undefined' ||
				typeof property.hypernyms[0] === 'undefined' ||
				property.hypernyms[0].synsetid === '');
			});
			properties.forEach(function(property) {
				for (var h = 0, stop = false; h < property.hypernyms.length && !stop; h++) {
					$scope.matches.forEach(function(match) {
						if (match.synset !== property.hypernyms[h].synsetid) return;
						var subtree = {
							word: property.hypernyms[h].words[0].lemma,
							synset: property.hypernyms[h].synsetid,
							branch: [property].concat(property.hypernyms.slice(0, h)),
							root: property.hypernyms[h],
							depth: property.hypernyms.length - h,
							penalty: 10 * $scope.isInAPenalizingCategory(property),
							wordSentence: $scope.findSentenceWordCameFrom(property.word, '.', $scope.concepts[concept].wikipediaAbstract)
						};
						subtree.compatibility = subtree.depth - subtree.branch.length - subtree.penalty;
						subtrees.push(subtree);
						stop = true;
					});
				}
			});

			subtrees = subtrees.sort(function (a, b) {
				return (a.compatibility < b.compatibility) ? 1 :
				(a.compatibility > b.compatibility ? -1 : 0);
			});
			console.log(subtrees);
			return subtrees;
		};

		$scope.swapTrees = function() {
			var swappedSubtrees = [[],[]];

			for (var concept = 0; concept < 2; concept++) {
				for (var i = 0; i < $scope.activatedForBlending[concept].length; i++) {
					if ($scope.activatedForBlending[concept][i]) //if activated
						swappedSubtrees[1-concept].push($scope.subtrees[concept][i]); //Move subtrees to the other
					else
						swappedSubtrees[concept].push($scope.subtrees[concept][i]); //Keep subtree
				}
			}

			$scope.swappedSubtrees = swappedSubtrees;
			//return swappedSubtrees;
		};

		$scope.makeBestTree = function() {
			var swappedSubtrees = [[],[]];

			for (var concept = 0; concept < 2; concept++) {
				for (var i = 0; i < $scope.activatedForBlending[concept].length; i++) {
					if ($scope.activatedForBlending[concept][i]) { //if activated
						swappedSubtrees[0].push($scope.subtrees[concept][i]); //Move subtrees to the best tree
						$scope.conceptToKeepInBlend = 1-concept; //Keep the other concept HACK
					}
				}
			}

			$scope.swappedSubtrees = swappedSubtrees;
		};

		$scope.findSentenceWordCameFrom = function(word, split, text) {
			var sentences = text.split(split);
			var sentence = '';
			sentences.forEach(function(s) {
				if (s.indexOf(word) !== -1) {
					sentence = s.split(word).join('|'+ word + '|');
					return;
				}
			});
			if (split === ',') return sentence || word + ' sentence not found';
			if (sentence.length > 20) return $scope.findSentenceWordCameFrom(word, ',', sentence) || word + ' sentence not found';
			return sentence || word + ' sentence not found';
		};

		var joinSubtree = function(subtree, rootObject, treeHeight, concept, i) {
			var root = rootObject;
			subtree = root;
			var properties = $scope.concepts[concept].properties;
			for (var j = treeHeight - 1; j >= 0; j--) { //Push branch nodes
				var nextRoot = {
					word: properties[i].hypernyms[j].words[0].lemma,
					synset: properties[i].hypernyms[j].synsetid
				};
				root[properties[i].hypernyms[j].synsetid] = nextRoot;
				root = nextRoot;
			}
			root.leaf = { //Push leaf node
				word: properties[i].word,
				synset: properties[i].synsets[properties[i].topSynset].synsetID
			};
		};

		var synsetIsInArray = function(synsetID, array) {
			for (var i = 0; i < array.length; i++) {
				if (array[i].synset === synsetID) return true;
			}
			return false;
		};

		var getDefinition = function(synsetString, index) {
			var str = synsetString;
			for (var i = 0; i < index; i++) {
				if (str.indexOf('_') !== -1) str = str.slice(str.indexOf('_') + 1,str.length);
			}
			if (str.indexOf('_') === -1) return str.slice(str.indexOf('-') + 2);
			return str.slice(str.indexOf('-') + 2,str.indexOf('_'));
		};

		var setOperations = function(arr1, arr2) {
			var seen = {};
			var symmetricDifference = [];
			var intersection = [];
			arr1.forEach(function(val) {
				seen[val.synset] = {'seen':1 , 'value': val}; // we saw it on first array
			});
			arr2.forEach(function(val) {
				if (typeof seen[val.synset] !== 'undefined' && seen[val.synset].seen === 1) { // we already saw it on the first one, unmark
					seen[val.synset] = {'seen':false , 'value': val};
				} else //if (seen.hasOwnProperty(seen[val.synset].seen) === false) { // if we hadnt seen it earlier
					{
					seen[val.synset] = {'seen':2 , 'value': val}; // mark with a 2
				}
			});

			for (var val in seen) {
				if (seen[val].seen) // if its a 1 or a 2, it was unique
					symmetricDifference.push(seen[val].value);
				else
					intersection.push(seen[val].value);
			}
			return {'symmetricDifference':symmetricDifference, 'intersection':intersection};
		};

	}
]);
