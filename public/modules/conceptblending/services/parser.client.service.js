'use strict';

angular.module('conceptblending').factory('Parser', [
	function() {
		// Parser service logic
		// ...

		// Public API
		return {
			getAbstractFromWikipediaArticle: function(text) {
				var pText = '';
				for (var p in text) {
					//Remove html comment
					text[p] = text[p].split('<!--');
					if (text[p].length > 1) {
						text[p][0] = text[p][0].split(/\r\n|\r|\n/);
						text[p][0] = text[p][0][0];
						text[p][0] += '</p> ';
					}
					text[p] = text[p][0];

					//Construct a string from paragraphs
					if (text[p].indexOf('</p>') === text[p].length - 5) {
						var htmlStrip = text[p].replace(/<(?:.|\n)*?>/gm, ''); //Remove HTML
						var splitNewline = htmlStrip.split(/\r\n|\r|\n/); //Split on newlines
						for (var newline in splitNewline) {
							if (splitNewline[newline].substring(0, 11) !== 'Cite error:') {
								pText += splitNewline[newline];
								pText += '\n';
							}
						}
					}
				}
				return pText;
			},
			removeWikipediaArtifacts: function(pText) {
				pText = pText.substring(0, pText.length - 2); //Remove extra newline

				pText = pText.replace(/\r?\n|\r/g, ' '); //Replace line breaks with whitespaces
				pText = pText.replace(/\//g, ' '); //Split words separated by forward slash
				pText = pText.replace(/\&#160;/g, ''); //Remove &#160; to avoid the number 160 appearing in the text
				//pText = pText.replace(/\[\d+\]/g, ''); //Remove reference tags (e.x. [1], [4], etc)
				pText = pText.replace(/\[.*?\]/g, ''); //Remove [] and the containing text
				//document.getElementById('textarea').value = pText
				pText = pText.replace(/\(.*?\)/g, ''); //Remove paranthesis and the containing text
				pText = pText.replace(/\/.*?\//g, ''); //Remove forward-slash and the containing text (usually phonetics)

				pText = pText.replace(/\-\B/g, ''); //Remove - at the end of words

				pText = pText.replace(/\bd\'\b/g, ''); //Remove d' at the start of french words
				pText = pText.replace(/\bl\'\b/g, ''); //Remove l' at the start of french words
				return pText;
			},
			removeSymbolsWordNetWontAccept: function(pText) {
				//pText = pText.replace(/\'\B/g, ''); //Remove ' at the end of words
				pText = pText.replace(/\'s\b/g, ''); //Remove 's at the end of words
				pText = pText.replace(/\'/g, ''); //Remove all apostrophes

				//pText = pText.replace(/[^a-z0-9 -]/gi,''); //Remove symbols
				pText = pText.replace(/[!@#\$%\^&\*\(\)\{\}\?<>\+:;",\.\\♭♯£€¤=`_~]/g,''); //Remove symbols by filtering
				//pText = pText.replace(/[a-z]*\d+[a-z]*/gi, ''); //Remove words that has numbers in it (symbols counts as word separator)
				pText = pText.replace(/[^\s]*\d+[^\s]*/gi, ''); //Remove words that has numbers in it

				pText = pText.toLowerCase(); //Wordnet requires lowercase string

				return pText;
			},
			removeDuplicateWords: function(text) {
				var removeDuplicateElements = function(elem) {
						if (this.indexOf(elem) === -1) this.push(elem);
						else console.log('Removing duplicate ' + elem);
				};

				//Remove duplicate words
				var uniqueWords = [];
				text.split(' ').forEach(removeDuplicateElements, uniqueWords);
				return uniqueWords.join(' ');
			}
		};
	}
]);
