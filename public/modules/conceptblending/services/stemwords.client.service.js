'use strict';

angular.module('conceptblending').factory('Stemwords', [
	function() {
		// Stemwords service logic
		// ...

		// Public API
		return {
			stemAllWords: function(text) {
				/*
					Requires a string of words separated by whitespaces
				*/
				/*var stemmer = require('stemmer');
				var newString = '';
				for (var word in text.split(' ')) {
					if (newString !== '') newString += ' ';
					newString += stemmer(word);
				}
				return newString;*/
				return text;
			}
		};
	}
]);
