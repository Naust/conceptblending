'use strict';

angular.module('conceptblending').factory('Tree', [
	function() {
		// Tree service logic
		// ...

		// Public API
		return {
			//Find out if object has an element where queryKey.value === queryVal
			searchObjectForSubtree: function searchObjectForSubtree(obj, queryVal, queryKey) {
			  for (var key in obj) {
			    var value = obj[key];
			    if (typeof value === 'object' && searchObjectForSubtree(value, queryVal, queryKey)) return true;
			    else if (key === queryKey && value === queryVal) return true;
			  }
			  return false;
			},

			//Search object for an element where queryKey.value === queryVal
			findElementInObject: function findElementInObject(obj, queryVal, queryKey) {
				var flag = -1;
				for (var key in obj) {
					var value = obj[key];
					if (typeof value === 'object') {
						if (value[queryKey] === queryVal) return value;
						if (flag === -1) flag = findElementInObject(value, queryVal, queryKey);
					}
					else if (key === queryKey && value === queryVal) flag = obj;
				}
				return flag;
			},

			findIndexOfElementInObject: function findIndexOfElementInObject(obj, queryVal, queryKey, i) {
				var index = -1;
				for (var key in obj) {
					var value = obj[key];
					if (typeof value === 'object') {
						if (value[queryKey] === queryVal) return i;
						if (index === -1) index = findIndexOfElementInObject(value, queryVal, queryKey, i+1);
					}
					else if (key === queryKey && value === queryVal) index = i;
				}
				return index;
			},

			synsetIsInTree: function(synset, tree) {
				for (var i = 0; i < tree.nodes.length; i++) {
					if (tree.nodes[i].id === synset) return true;
				}
				return false;
			},

			hypernymIsInTree: function(hypernyms, tree) {
				if (typeof hypernyms === 'undefined' || hypernyms.length === 0) return false;

				for (var i = 0; i < tree.nodes.length; i++) {
					for (var j = 0; j < hypernyms.length; j++) {
						if (tree.nodes[i].id === hypernyms[j].synsetid) return true;
					}
				}
				return false;
			},

			degreeOfHypernymInTree: function(hypernyms, tree) {
				for (var i = 0; i < hypernyms.length; i++) {
					for (var j = 0; j < tree.nodes.length; j++) {
						if (tree.nodes[j].id === hypernyms[i].synsetid) {
							return i;
						}
					}
				}

				return -1;
			},

			getHyponymsAsTree: function(property, degree) {
				var synsetID = property.synsets[property.topSynset].synsetID;
				var hypernyms = property.hypernyms;

				var nodes = [];
				var edges = [];

				//Attach synset as lowest hyponym
				nodes.push({
					id: synsetID,
					//label: String(synsetID) + ' ' + synsetWord
					label: property.word,
					//color: 'red'
					color: {
						border: 'green',
						background: 'lime'
					}
				});
				edges.push({
					from: synsetID,
					to: hypernyms[0].synsetid
				});

				for (var i = 0; i < degree; i++) {
					nodes.push({
						id: hypernyms[i].synsetid,
						//label: String(hypernymIDs[i]) + ' ' + hypernymWords[i]
						label: hypernyms[i].words[0].lemma
					});
					edges.push({
						from: hypernyms[i].synsetid,
						to: hypernyms[i + 1].synsetid
					});
				}

				return {nodes:nodes, edges:edges};
			},

			createNewTree: function(property) {
				var nodes = [];
				var edges = [];

				var synsetID = property.synsets[property.topSynset].synsetID;
				var hypernyms = property.hypernyms;

				nodes.push({
					id: synsetID,
					//label: String(synsetID) + ' ' + synsetWord
					label: property.word,
					//color: 'red'
					color: {
						border: 'green',
						background: 'lime'
					}
				});
				if (typeof hypernyms === 'undefined' || hypernyms.length === 0) return {nodes:nodes, edges:edges};

				edges.push({
					from: property.synsets[property.topSynset].synsetID,
					to: hypernyms[0].synsetid
				});

				var from, to, fromWord;
				for (var h = 0; h < hypernyms.length; h++) {
					from = hypernyms[h].synsetid;
					fromWord = hypernyms[h].words[0].lemma;
					nodes.push({
						id: from,
						//label: String(from) + ' ' + fromWord
						label: fromWord
						//color: 'orange'
					});

					if (h < hypernyms.length - 1) {
						to = hypernyms[h+1].synsetid;
						edges.push({
							from: from,
							to: to
						});
					}
				}
				return {nodes:nodes, edges:edges};
			},

			createNodes: function(subtrees) {
				var nodes = [];
				subtrees.forEach(function(subtree) {
					subtree.branch.forEach(function(element) {
						nodes.push({
							id: element.synsetid !== undefined ? element.synsetid : element.synsets[element.topSynset].synsetID,
							label: element.word !== undefined ? element.word : element.words[0].lemma,
							color: {
								border: 'black',
								background: 'orange'
							}
						});
					});
				});
				return nodes;
			},

			createEdges: function(subtrees) {
				var edges = [];
				subtrees.forEach(function(subtree) {
					var child;
					subtree.branch.forEach(function(element) {
						var synset = element.synsetid !== undefined ? element.synsetid : element.synsets[element.topSynset].synsetID;
						if (typeof child !== 'undefined')
							edges.push({
								from: child,
								to: synset
							});
						child = synset;
					});
					edges.push({ //Connect subtree to generic tree
						from: child,
						to: subtree.root.synsetid
					});
				});
				return edges;
			},

			createNodeString: function createNodeString(obj) {
				var nodes = [];
				for (var key in obj) {
					var value = obj[key];
					if (typeof value === 'object') nodes = nodes.concat(createNodeString(value));
					else if (key === 'synset') nodes.push(obj.word);
				}
				return nodes;
			},

			propertyHasHypernymWord: function(property, hypernymWord) {
				if (property === null) return false;
				var hasHypernymWord = false;
				property.hypernyms.forEach(function(hypernym) {
					hypernym.words.forEach(function(word) {
						if (word.lemma === hypernymWord)
							hasHypernymWord = true;
					});
				});
				return hasHypernymWord;
			},

			propertyHasHypernym: function(property, hypernymSynset) {
				if (property === null) return false;
				var hasHypernym = false;
				property.hypernyms.forEach(function(hypernym) {
					if (hypernym.synsetid === hypernymSynset)
						hasHypernym = true;
				});
				return hasHypernym;
			},
			}

		};
	}
]);
