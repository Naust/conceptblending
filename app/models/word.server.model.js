'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Word Schema
 */
var WordSchema = new Schema({
	// Word model fields   
	// ...
});

mongoose.model('Word', WordSchema);