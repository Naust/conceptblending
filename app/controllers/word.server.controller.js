'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    _ = require('lodash');

/**
 * Create a Word
 */
exports.create = function(req, res) {

};

/**
 * Show the current Word
 */
exports.read = function(req, res) {

};

/**
 * Update a Word
 */
exports.update = function(req, res) {

};

/**
 * Delete an Word
 */
exports.delete = function(req, res) {

};

/**
 * List of Words
 */
exports.list = function(req, res) {

};