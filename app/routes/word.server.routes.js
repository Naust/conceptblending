'use strict';

module.exports = function(app) {
	// Routing logic
	// ...

	var bodyParser = require('body-parser');
	app.use( bodyParser.json() );       // to support JSON-encoded bodies
	var wordNet = require('wordnet-magic');
	var wn = wordNet(null, true);
	var util = require('util');
	var async = require('async');
	var stemmer = require('stemmer');
	var pos = require('pos');
	var tagger = new pos.Tagger();
	var removeSynsetsWithWrongPos = true;
	var StanfordSimpleNLP = require('stanford-simple-nlp');
	var stanfordSimpleNLP = new StanfordSimpleNLP.StanfordSimpleNLP();
	stanfordSimpleNLP.loadPipelineSync({'annotators': ['tokenize','ssplit','parse','pos']});

	//Find synsets and hypernymtree (where correct synset is found) in text
	app.post('/synsets',function(req,res) {
		if (!req.body) return res.sendStatus(400).send('Error: no body (synsets)');
		var propertyWords = req.body.propertyWords;
		var sendSynsets = new Array(propertyWords.length);
		var sendLemmas = new Array(propertyWords.length);
		var sendHypernyms = new Array(propertyWords.length);

		var taggedAbstract = [];
		var sendPOS;
		stanfordSimpleNLP.process(req.body.wikipediaAbstract, function(err, result) {
			//console.log(result.document.sentences.sentence);
			console.log(result);
			console.log(result.document.sentences.sentence[0].tokens.token[0].word);
			result.document.sentences.sentence.forEach(function(sentence){
				sentence.tokens.token.forEach(function(token) {
					taggedAbstract.push([token.word, token.POS]);
				})
			});
			sendPOS = getPOSTagsForEachProperty(propertyWords, taggedAbstract);

			async.forEachOfSeries(propertyWords, function(word, key, callback) { //For each word in the text
				sendSynsets[key] = [];
				sendLemmas[key] = '';
				sendHypernyms[key] = [];
				var stem = stemmer(word);
				var allLemmasCondensed = sendLemmas.join().split(', ').join().split(',');
				if (allLemmasCondensed.indexOf(word) !== -1 || allLemmasCondensed.indexOf(stem) !== -1) {
					sendSynsets[key] = { error: 'ERROR:duplicate' };
					return callback(null);
				}

				var wordNetWord = new wn.Word(word);

				async.series([
					function(callback) {
						wordNetWord.getSynsets(function(err, data) { //Find synsets of unstemmed word
							if (err) return callback(null);
							callback(null, data);
						});
					},
					function(callback) {
						if (word === stem) return callback(null); //Avoid looking up same word twice
						wordNetWord = new wn.Word(stem);
						wordNetWord.getSynsets(function(err, data) { //Find synsets of stemmed word
							if (err) return callback(null);
							callback(null, data);
						});
					}
				],
				function(err, results) {
					//MOTTA [UNSTEMMED,STEMMED]
					if (results[0] === undefined && results[1] === undefined) {
						sendSynsets[key] = { error: 'ERROR:no word data found' };
						return callback(null);
					}
					var theOnlySynset;
					if (results[0] !== undefined) {
						sendSynsets[key] = writeSynsets(results[0], [], sendPOS[key], removeSynsetsWithWrongPos);
						sendLemmas[key] = word;
						if (results[0].length === 1) theOnlySynset = results[0][0];
					}

					if (results[1] !== undefined) {
						if (results[1].length === 0 && sendSynsets[key].length === 0) {
							sendSynsets[key] = { error: 'ERROR:no synsets found' };
							return callback(null);
						}

						sendLemmas[key] = [sendLemmas[key], stem].filter(Boolean).join(', ');
						if (results[1].length === 1 && sendSynsets[key].length === 0) //We have only found one synset so we can find hypernyms right away
						theOnlySynset = results[1][0];
						sendSynsets[key] = sendSynsets[key].concat(writeSynsets(results[1], sendSynsets[key], sendPOS[key], removeSynsetsWithWrongPos));
					}

					console.log('finished', results);
					if (typeof theOnlySynset === 'undefined') return callback();
					theOnlySynset.getHypernymsTree().then(function(hypernymArray) {
						wn.print(hypernymArray);
						console.log(hypernymArray);

						sendHypernyms[key] = hypernymObjectToArray(hypernymArray[0]);
						callback();
					}, function(reason) {
						callback();
					});
				});
			}, function(err) {
				if (err) res.sendStatus(400).send(err);
				res.send({
					'synsets': sendSynsets,
					'lemmas': sendLemmas,
					'hypernyms': sendHypernyms,
					'pos' : sendPOS
				});
			});
		});
	});

	app.post('/hypernyms',function(req,res) {
		if (!req.body) return res.sendStatus(400).send('Error: no body (hypernyms)');
		var word = req.body.word;
		var synsetid = req.body.synsetid;
		var sendHypernyms;
		var currentSynset;
		var wordNetWord;
		var lookupStem = false;

		console.log(word + ' ' + synsetid);
		try {
			wordNetWord = new wn.Word(word);
		}
		catch (e) {
			console.error('Error word not found');
			return res.sendStatus(400).send('Error word not found');
		}

		async.series([
			function(callback) {
				wordNetWord.getSynsets(function(err, data) {
					if (err) {
						lookupStem = true;
						console.log('lookup stem');
						return callback(null, 'unstemmed');
					}
					console.log('found unstemmed');
					for (var i = 0; i < data.length; i++) {
						console.log(synsetid + ' == ' + data[i].synsetid,
						typeof synsetid + ' == ' + typeof data[i].synsetid,
						synsetid === data[i].synsetid);
						if (synsetid > 0 && synsetid === data[i].synsetid) {
							currentSynset = data[i];
							//console.log(currentSynset);
						}
					}
					lookupStem = true;
					callback(null, 'unstemmed');
				});
			},
			function(callback) {
				if (!lookupStem) return callback();
				try {
					var stem = stemmer(word);
					if (stem === word) return callback(null, 'stemmed'); //Avoid duplicate lemmas
					wordNetWord = new wn.Word(stem);
				}
				catch (e) {
					return callback(null, 'stemmed');
				}
				wordNetWord.getSynsets(function(err, data) {
					if (err) {
						console.log('Error word not found');
						return callback(null, 'stemmed');
					}

					for (var i = 0; i < data.length; i++) {
						if (synsetid > 0 && synsetid === data[i].synsetid)
							currentSynset = data[i];
							//console.log(currentSynset);
					}
					return callback(null, 'stemmed');
				});
			},
			function(callback) {
				if (typeof currentSynset === 'undefined') return callback(null, 'hypernym');
				currentSynset.getHypernymsTree().then(function(hypernymArray) {

						//console.log(currentSynset);
						wn.print(hypernymArray);
						//console.log(hypernymArray);

						sendHypernyms = hypernymObjectToArray(hypernymArray[0]);

						return callback(null, 'hypernym');
				}, function(reason) {
						return callback(null, 'hypernym');
				});
			}
		],
		function(err, results) {
			console.log('finished');
			console.log(results);
			res.send({
				'hypernyms': sendHypernyms
			});
		});
	});

	app.post('/nounphrases',function(req,res) {
		if (!req.body) return res.sendStatus(400).send('Error: no body (nounphrases)');

		stanfordSimpleNLP.process(req.body.wikipediaAbstract, function(err, result) {
			var nounPhrases = [];
			var sentence = result.document.sentences.sentence;
			if (!Array.isArray(sentence))
				nounPhrases = findNounPhrases(sentence.parse.split('\n'));
			else
				for (var i = 0; i < sentence.length; i++) {
					console.log(sentence[i].parse);
					var lines = sentence[i].parse.split('\n');
					nounPhrases = nounPhrases.concat(findNounPhrases(lines));
					console.log(nounPhrases);
				}

			res.send({
				'nounphrases': nounPhrases
			});
		});
	});

	var getPOSTagsForEachProperty = function(propertyWords, taggedWords) {
		var POSTags = [];
		var duplicates = [];
		//Find class for every word in text, to remove synsets of wrong class
		for (var i in taggedWords) {
			if (typeof taggedWords[i][0] === 'undefined') continue;
			var word = taggedWords[i][0].toLowerCase();
			var tag = taggedWords[i][1];
			console.log(word + ' /' + tag);
			if (duplicates.indexOf(word) === -1 && propertyWords.indexOf(word) !== -1) {
				POSTags.push(tag + '-' + word);
				//POSTags.push(tag);
				duplicates.push(word);
			}
		}

		if (POSTags.length !== propertyWords.length) { //If POSTags has wrong length, adjust it to avoid crashing the server
			console.log(POSTags, POSTags.length + ' !== ' + propertyWords.length);
			var oldLength = POSTags.length;
			POSTags.length = propertyWords.length;
			POSTags.fill('', oldLength, POSTags.length);
		}

		return POSTags;
	};

	var writeSynsets = function(data, duplicates, tag, removeSynsetsWithWrongPos) {
		var synsets = [];
		var wrongClassSynsets = [];
		data.forEach(function(d) {
			if (wrongClassSynsets.length === 0) wrongClassSynsets.push({
				synsetID: d.synsetid,
				lexdomain: d.lexdomain,
				definition: d.definition
			});
			if (duplicates.indexOf(d.synsetid) !== -1) return;//Avoid duplicate synsets
			if (removeSynsetsWithWrongPos && typeof tag[0] !== 'undefined') {
				if ((d.pos === 'n' && tag[0] !== 'N') || //Noun
				(d.pos === 'v' && tag[0] !== 'V') || //Verb
				(d.pos === 'a' && tag[0] !== 'J') || //Adjective
				(d.pos === 'r' && tag[0] !== 'R') ||//Adverb
				(d.pos === 's' && tag[0] !== 'J')) return; //Satellite adjective can be any adjective?
			}
			synsets.push({
				synsetID: d.synsetid,
				lexdomain: d.lexdomain,
				definition: d.definition
			});
		});
		//if (synsets === '') return wrongClassSynsets; //If no synset of right class is found, any synset is returned
		if (synsets.length === 0) return wrongClassSynsets; //If no synset of right class is found, any synset is returned
		return synsets;
	};

	var hypernymObjectToArray = function(hypernymObject) {
		if (typeof hypernymObject === 'undefined') return [];
		var hypernyms = [];
		console.log(hypernymObject);
		hypernyms.push(hypernymObject);
		while (typeof hypernymObject.hypernym !== 'undefined' && hypernymObject.hypernym.length > 0) {
			hypernyms.push(hypernymObject.hypernym[0]);
			hypernymObject = hypernymObject.hypernym[0];
			console.log(hypernyms);
		}
		hypernyms.forEach(function(hypernym) {
			delete hypernym.hypernym; //Delete excessive hypernym data
			//hypernym = 'undefined'; //Unlink hypernyms to avoid sending excessive data
		});
		return hypernyms;
	};

	var findNounPhrase = function(line) {
		if (line.indexOf('(NP') === -1) return null;
		console.log('line |' + line);
		var tags = line.substr(line.indexOf('(')).substr(5).split(') (');
		console.log(tags);
		if (tags.length < 2) return null;
		tags[tags.length - 1] = tags[tags.length - 1].replace(/[\)\r]/g, '');
		var nounPhrase = '';
		for (var wordNr = 0; wordNr < tags.length; wordNr++) {
			var tag = tags[wordNr].split(' ');
			if (tag[0].match(/[a-z]/i) === null) continue; //Remove symbols
			if (tag[0] === 'POS') continue; //Remove possessive endings
			if (tag[0] === 'DT') continue; //Remove determinants
			if (nounPhrase !== '') nounPhrase += ' ';
			nounPhrase += tag[1];
			console.log('Tag |' + tag[0] + '| |' + tag[1] + '| = |' + nounPhrase + '|');
		}
		if (nounPhrase.split(' ').length < 2) return null;
		return nounPhrase;
	};

	var findNounPhrases = function(lines) {
		var nPs = [];

		for (var lineNr = 0; lineNr < lines.length; lineNr++) {
			var nounPhrase = findNounPhrase(lines[lineNr]);
			if (nounPhrase === null) continue;
			nPs.push(nounPhrase);
			while (nounPhrase.split(' ').length > 2) { //Add tail noun phrases until only 2 words left
				nounPhrase = nounPhrase.substr(nounPhrase.indexOf(' ') + 1);
				nPs.push(nounPhrase);
			}
		}
		return nPs;
	};


	//Take a tree of hypernyms (hypernym.hypernym. ... .hypernym)
	//and parse it into a single-line string,
	//where synonyms of hypernyms are separated by ,
	//and hypernym parents are separated by <
	var parseHypernymsTree = function(hypernymArray) {
		var hypernymString = '';
		function getSynsetString(input, depth) {
			var current_depth = depth + 1;
			var words;

			if (Array.isArray(input.words)) {
				words = input.words.map(function(item) {
					return item.lemma;
				});
			} else {
				words = new Array(input.lemma);
			}

			var str = words.join(', ');

			if (input.hypernym) {
				if (Array.isArray(input.hypernym)) {
					input.hypernym.forEach(function(elem) {
						var space = String(' ');
						var times = current_depth * 4;
						var spaces = space.repeat(times);
						str += ' < ' + getSynsetString(elem, current_depth);
					});
				} else {
					str += ' < '+ getSynsetString(input.hypernym, current_depth);
				}
			}

			if (input.hyponym) {
				if(Array.isArray(input.hyponym)) {
					input.hyponym.forEach(function(elem) {
						var space = String(' ');
						var times = current_depth * 4;
						var spaces = space.repeat(times);
						str += ' < ' + getSynsetString(elem, current_depth);
					});
				} else {
					str += ' < ' + getSynsetString(input.hyponym, current_depth);
				}
			}
			return str;
		}

		if (typeof hypernymArray[0] !== 'undefined') {
			hypernymString = '';
			if(Array.isArray(hypernymArray))
				hypernymString += getSynsetString(hypernymArray[0], 0);
			else
				hypernymString += getSynsetString(hypernymArray, 0);
		}
		if (hypernymString === '') return [];
		return hypernymString.split(' < ');
	};

	//Take a tree of hypernym synsets (hypernym.hypernym. ... .hypernym)
	//and parse it into a single-line string,
	//where synonyms of hypernyms are separated by ,
	//and hypernym parents are separated by <
	//same as above function, except synsetIDs instead of lemmas
	var parseHypernymsTreeSynsets = function(hypernymArray) {
		function getSynsetString(input, depth) {
			var current_depth = depth + 1;
			var str = input.synsetid;

			if (input.hypernym) {
				if (Array.isArray(input.hypernym)) {
					input.hypernym.forEach(function(elem) {
						var space = String(' ');
						var times = current_depth * 4;
						var spaces = space.repeat(times);
						str += ' < ' + getSynsetString(elem, current_depth);
					});
				} else {
					str += ' < '+ getSynsetString(input.hypernym, current_depth);
				}
			}

			if (input.hyponym) {
				if (Array.isArray(input.hyponym)) {
					input.hyponym.forEach(function(elem) {
						var space = String(' ');
						var times = current_depth * 4;
						var spaces = space.repeat(times);
						str += ' < ' + getSynsetString(elem, current_depth);
					});
				} else {
					str += ' < ' + getSynsetString(input.hyponym, current_depth);
				}
			}
			return str;
		}

		var hypernymString = '';
		if (typeof hypernymArray[0] !== 'undefined') {
			if(Array.isArray(hypernymArray))
				hypernymString += getSynsetString(hypernymArray[0], 0);
			else
				hypernymString += getSynsetString(hypernymArray, 0);
		}

		if (hypernymString === '') return undefined;
		return getHypernymsArray(hypernymString);
	};

	var getHypernymsArray = function(hypernymSynsets) {
		var hypernymArray = [];
		var str = hypernymSynsets;
		var numberOfHypernyms = (str.match(/</g) || []).length + 1;
		for (var i = 0; i < numberOfHypernyms; i++) {
			if (str.indexOf('<') === -1) {
				hypernymArray.push(str);
				return hypernymArray;
			}
			else {
				hypernymArray.push(str.substring(0, str.indexOf('<') - 1));
				str = str.slice(str.indexOf('<') + 2,str.length);
			}
		}
		if (hypernymArray.length === 0) return undefined;
		return hypernymArray;
	};

};
